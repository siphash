(define-library (foldling siphash)
  (import (scheme base)
          (scheme case-lambda))
  (cond-expand
    ((library (srfi 33)) (import (srfi 33)))
    ((library (srfi 60)) (import (srfi 60)))
    (chicken
     (import (only (chicken) bitwise-ior bitwise-xor bitwise-and arithmetic-shift)
             (numbers)))
    (else
     (error "One of (srfi 33) or (srfi 60) is required")))
  (export make-siphash siphash-2-4 siphash-4-8)
  (include "siphash.scm"))

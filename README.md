siphash
=======
A Scheme implementation of the SipHash family of hash functions.

Description
-----------
[SipHash][] is a cryptographically strong family of hash functions designed
by Jean-Philippe Aumasson and Daniel J. Bernstein.

This library provides an implementation of these functions for R7RS and
[CHICKEN Scheme][].

[SipHash]: http://131002.net/siphash
[CHICKEN Scheme]: http://call-cc.org/

Installation
------------
To install for CHICKEN Scheme, just run `chicken-install`:

    $ git clone http://git.foldling.org/siphash.git
    $ cd siphash
    $ chicken-install -test

On other R7RS Schemes, this project's `src/` directory should be added
or moved onto the system's load path.

Usage
-----
The `(foldling siphash)` library provides three functions:

  * `make-siphash`
  * `siphash-2-4`
  * `siphash-4-8`

`make-siphash` constructs a hashing function.

It takes two positive integer arguments `c` and `d` and returns a
hashing procedure of that number of compression and finalization rounds,
respectively.

    (make-siphash c d) => (procedure key) => (procedure message) => integer

`siphash-2-4` and `siphash-4-8` are predefined hashing procedures.

Each takes one or two arguments, the key and message to hash, and
returns a positive integer. `key` and `message` are bytevectors, and
`key` must have a length of 16 while `message` may be any length. If
`message` isn't given, a prekeyed hashing function is returned.

The SipHash specification recommends SipHash-2-4 for performance and
SipHash-4-8 for cryptographic security.

    (siphash-2-4 key) => (procedure message) => integer
    (siphash-2-4 key message) => integer
    (siphash-4-8 key) => (procedure message) => integer
    (siphash-4-8 key message) => integer

Examples
--------

    > (import (foldling siphash))
    > (define key (bytevector 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))
    > (define msg (string->utf8 "The rain in Spain falls mainly on the plain."))
    
    > (siphash-2-4 key msg)
    ; => 8751579407287093977
    
    > ((siphash-4-8 key) msg)
    ; => 13472556437817646137
    
    > ((make-siphash 8 16)
       key
       (string->utf8
        "In Hertford, Hereford and Hampshire, hurricanes hardly ever happen."))
    ; => 9275736844991428064

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
Copyright (c) 2013-2018, Evan Hanson under a BSD-style license.

See LICENSE for full license information.
